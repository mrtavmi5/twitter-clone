package com.grubisa.twitterclone.service;

import java.util.ArrayList;
import java.util.List;

import com.grubisa.twitterclone.exception.PostNotFoundException;
import com.grubisa.twitterclone.model.Notification;
import com.grubisa.twitterclone.model.Post;
import com.grubisa.twitterclone.model.User;
import com.grubisa.twitterclone.repository.NotificationRepository;
import com.grubisa.twitterclone.repository.PostRepository;
import com.grubisa.twitterclone.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService {

    @Autowired
    PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    NotificationRepository notificationRepository;

    public List<Post> findAll() {
        return postRepository.findAll();
    }

    public Post findById(Long id) throws PostNotFoundException {
        return postRepository.findById(id).orElseThrow(() -> new PostNotFoundException(id));
    }

    public List<Post> findByContent(String content) {
        return postRepository.findByContentContaining(content);
    }

    public void delete(Long id) {
        postRepository.deleteById(id);
    }

    public Post save(Post post, String username) {
        User user = userRepository.findByUsername(username);
        post.setUser(user);
        post = postRepository.save(post);
        List<User> followers = user.getFollowers();
        List<Notification> notifications = new ArrayList<>();

        for (User user2 : followers) {
            int unseenNotifications = user2.getUnseenNotifications();
            unseenNotifications++;
            user2.setUnseenNotifications(unseenNotifications);
            Notification notification = new Notification(null, user2, post);
            notifications.add(notification);
        }

        notificationRepository.saveAll(notifications);
        userRepository.saveAll(followers);
        return post;
    }
}