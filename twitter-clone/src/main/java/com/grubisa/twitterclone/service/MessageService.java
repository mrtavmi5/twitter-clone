package com.grubisa.twitterclone.service;

import java.util.List;

import com.grubisa.twitterclone.exception.MessageNotFoundException;
import com.grubisa.twitterclone.model.Message;
import com.grubisa.twitterclone.repository.MessageRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public Message findById(Long id) throws MessageNotFoundException {
        return messageRepository.findById(id).orElseThrow(() -> new MessageNotFoundException(id));
    }

    public List<Message> findByUsername(String username) {
        return messageRepository.findByUserUsername(username);
    }

    public void delete(Long id) {
        messageRepository.deleteById(id);
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }
}