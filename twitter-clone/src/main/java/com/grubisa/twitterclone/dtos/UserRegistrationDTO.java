package com.grubisa.twitterclone.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import com.grubisa.twitterclone.annotations.ValidPassword;
import lombok.Data;

@Data
public class UserRegistrationDTO {

    @NotNull
    @NotBlank
    @Pattern(regexp = "^([A-Za-z0-9])+$")
    private String username;
    @NotNull
    @NotBlank
    @ValidPassword
    private String password;
    @NotNull
    @NotBlank
    @ValidPassword
    private String confirmPassword;
}
