package com.grubisa.twitterclone.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
public class UserDTO {

    private Long id;
    @NotNull
    @NotBlank
    @Pattern(regexp = "^([A-Za-z0-9])+$")
    private String username;
}
