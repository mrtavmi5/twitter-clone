package com.grubisa.twitterclone.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class PostDTO {

    private Long id;
    @NotNull
    @NotBlank
    private String content;
}