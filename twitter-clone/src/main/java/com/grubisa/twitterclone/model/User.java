package com.grubisa.twitterclone.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private int unseenNotifications;

    @OneToMany(fetch = FetchType.LAZY)
    private List<User> following;
    @OneToMany(fetch = FetchType.LAZY)
    private List<User> followers;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Post> posts;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Inbox> inbox;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Notification> notifications;

}