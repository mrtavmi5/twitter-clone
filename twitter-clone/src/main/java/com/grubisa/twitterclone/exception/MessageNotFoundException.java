package com.grubisa.twitterclone.exception;

public class MessageNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public MessageNotFoundException(Long id) {
        super("Message with this " + id + " not found!");
    }
}