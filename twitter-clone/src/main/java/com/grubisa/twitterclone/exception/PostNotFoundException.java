package com.grubisa.twitterclone.exception;

public class PostNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public PostNotFoundException(Long id) {
        super("Post with this " + id + " not found!");
    }
}