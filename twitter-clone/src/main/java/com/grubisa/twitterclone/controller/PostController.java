package com.grubisa.twitterclone.controller;

import java.util.List;

import javax.validation.Valid;

import com.grubisa.twitterclone.dtos.PostDTO;
import com.grubisa.twitterclone.exception.PostNotFoundException;
import com.grubisa.twitterclone.model.Post;

import com.grubisa.twitterclone.service.PostService;
import com.grubisa.twitterclone.support.PostDTOToPost;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/post")
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @Autowired
    PostDTOToPost postDTOToPost;

    @GetMapping(value = "/all")
    public List<Post> getAll() {
        return postService.findAll();
    }

    @GetMapping(value = "/{id}")
    public Post getOne(@PathVariable Long id) throws PostNotFoundException {
        return postService.findById(id);
    }

    @GetMapping(value = "/{content}")
    public List<Post> getByContent(@PathVariable String content) {
        return postService.findByContent(content);
    }

    @DeleteMapping(value = "/{id}")
    public void deletePost(@PathVariable Long id) {
        postService.delete(id);
    }

    @PostMapping
    public Post addPost(@Valid @RequestBody PostDTO postDTO, @AuthenticationPrincipal User user) {
        Post post = postDTOToPost.convert(postDTO);
        return postService.save(post, user.getUsername());
    }
}