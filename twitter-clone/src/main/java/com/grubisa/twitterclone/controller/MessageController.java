package com.grubisa.twitterclone.controller;

import java.util.List;

import javax.validation.Valid;

import com.grubisa.twitterclone.exception.MessageNotFoundException;
import com.grubisa.twitterclone.model.Message;
import com.grubisa.twitterclone.service.MessageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping(value = "/all")
    public List<Message> getAll() {
        return messageService.findAll();
    }

    @GetMapping(value = "/{id}")
    public Message getOne(@PathVariable Long id) throws MessageNotFoundException {
        return messageService.findById(id);
    }

    @GetMapping(value = "/{username}")
    public List<Message> getByUsername(@PathVariable String username) {
        return messageService.findByUsername(username);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteMessage(@PathVariable Long id) {
        messageService.delete(id);
    }

    @PostMapping
    public Message addMessage(@Valid @RequestBody Message message) {
        return messageService.save(message);
    }
}