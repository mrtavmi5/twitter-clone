package com.grubisa.twitterclone.controller;

import com.grubisa.twitterclone.dtos.UserDTO;
import com.grubisa.twitterclone.dtos.UserRegistrationDTO;
import com.grubisa.twitterclone.exception.UserExistsException;
import com.grubisa.twitterclone.model.AuthenticationRequest;
import com.grubisa.twitterclone.model.AuthenticationResponse;
import com.grubisa.twitterclone.model.User;
import com.grubisa.twitterclone.security.MyUserDetailsService;
import com.grubisa.twitterclone.service.UserService;
import com.grubisa.twitterclone.support.UserRegistrationDTOToUser;
import com.grubisa.twitterclone.support.UserToUserDTO;
import com.grubisa.twitterclone.util.JwtUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserToUserDTO userToUserDTO;

    @Autowired
    private UserRegistrationDTOToUser userRegistrationDTOToUser;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
            throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

    @PostMapping(value = "/register")
    @ResponseStatus(code = HttpStatus.CREATED)
    public UserDTO create(@RequestBody @Validated UserRegistrationDTO userRegistrationDTO) throws UserExistsException {

        User existingUser = userService.findByUsername(userRegistrationDTO.getUsername());

        if (existingUser != null) {
            throw new UserExistsException(existingUser.getUsername());
        }

        User user = userRegistrationDTOToUser.convert(userRegistrationDTO);
        user = userService.save(user);
        return userToUserDTO.convert(user);
    };
}
