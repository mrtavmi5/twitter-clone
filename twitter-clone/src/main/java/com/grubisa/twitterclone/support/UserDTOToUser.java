package com.grubisa.twitterclone.support;

import java.util.ArrayList;
import java.util.List;

import com.grubisa.twitterclone.dtos.UserDTO;
import com.grubisa.twitterclone.model.User;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDTOToUser implements Converter<UserDTO, User> {

    @Override
    public User convert(UserDTO arg0) {
        User user = new User();
        user.setId(arg0.getId());
        user.setUsername(arg0.getUsername());
        return user;
    }

    public List<User> convert(List<UserDTO> dtos) {
        List<User> users = new ArrayList<User>();
        for (UserDTO dto : dtos) {
            users.add(convert(dto));
        }
        return users;
    }
}