package com.grubisa.twitterclone.support;

import com.grubisa.twitterclone.dtos.PostDTO;
import com.grubisa.twitterclone.model.Post;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PostToPostDTO implements Converter<Post, PostDTO> {

    @Override
    public PostDTO convert(Post arg0) {
        PostDTO dto = new PostDTO();
        dto.setId(arg0.getId());
        dto.setContent(arg0.getContent());
        return dto;
    }

}