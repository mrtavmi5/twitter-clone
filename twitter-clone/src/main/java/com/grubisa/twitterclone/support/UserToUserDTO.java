package com.grubisa.twitterclone.support;

import java.util.ArrayList;
import java.util.List;

import com.grubisa.twitterclone.dtos.UserDTO;
import com.grubisa.twitterclone.model.User;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDTO implements Converter<User, UserDTO> {

    @Override
    public UserDTO convert(User arg0) {
        UserDTO dto = new UserDTO();
        dto.setId(arg0.getId());
        dto.setUsername(arg0.getUsername());

        return dto;
    }

    public List<UserDTO> convert(List<User> users) {
        List<UserDTO> dtos = new ArrayList<UserDTO>();
        for (User user : users) {
            dtos.add(convert(user));
        }
        return dtos;
    }
}