package com.grubisa.twitterclone.support;

import java.util.ArrayList;
import java.util.List;

import com.grubisa.twitterclone.dtos.UserRegistrationDTO;
import com.grubisa.twitterclone.model.User;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserRegistrationDTOToUser implements Converter<UserRegistrationDTO, User> {

    @Override
    public User convert(UserRegistrationDTO arg0) {
        User user = new User();
        user.setId(0L);
        user.setUsername(arg0.getUsername());
        user.setPassword(arg0.getPassword());

        return user;
    }

    public List<User> convert(List<UserRegistrationDTO> dtos) {
        List<User> users = new ArrayList<User>();
        for (UserRegistrationDTO dto : dtos) {
            users.add(convert(dto));
        }
        return users;
    }
}