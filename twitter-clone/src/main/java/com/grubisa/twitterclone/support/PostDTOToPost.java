package com.grubisa.twitterclone.support;

import com.grubisa.twitterclone.dtos.PostDTO;
import com.grubisa.twitterclone.model.Post;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PostDTOToPost implements Converter<PostDTO, Post> {

    @Override
    public Post convert(PostDTO arg0) {
        Post post = new Post();
        post.setId(arg0.getId());
        post.setContent(arg0.getContent());
        return post;
    }

}