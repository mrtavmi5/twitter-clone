package com.grubisa.twitterclone.repository;

import java.util.List;

import com.grubisa.twitterclone.model.Message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findByUserUsername(String username);
}