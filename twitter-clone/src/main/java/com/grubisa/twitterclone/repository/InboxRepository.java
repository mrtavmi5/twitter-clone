package com.grubisa.twitterclone.repository;

import com.grubisa.twitterclone.model.Inbox;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InboxRepository extends JpaRepository<Inbox, Long> {

}