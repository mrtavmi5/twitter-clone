package com.grubisa.twitterclone.repository;

import java.util.List;

import com.grubisa.twitterclone.model.Post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findByContentContaining(String content);
}