import React, { Component } from "react";
import FollowCell from "./FollowCell";

class WhoToFollow extends Component {
  render() {
    return (
      <div>
        <label style={{ fontWeight: "bold" }}>WHO TO FOLLOW</label>
        <FollowCell />
        <FollowCell />
        <FollowCell />
      </div>
    );
  }
}

export default WhoToFollow;
