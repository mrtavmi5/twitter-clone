import React, { Component } from "react";
import Avatar from "./Avatar";

class FollowCell extends Component {
  render() {
    return (
      <div className="row">
        <div className="col">
          <Avatar />
        </div>
        <div className="col">Username</div>
        <div className="col">
          <button type="button" className="btn btn-primary">
            Follow
          </button>
        </div>
      </div>
    );
  }
}
export default FollowCell;
