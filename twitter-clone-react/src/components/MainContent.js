import React, { Component } from "react";
import SendTweet from "./SendTweet";
import Feed from "./Feed";

class MainContent extends Component {
  render() {
    return (
      <div
        className="col-8"
        style={{ borderColor: "red", borderWidth: 1, borderStyle: "solid" }}
      >
        <SendTweet />
        <Feed />
      </div>
    );
  }
}

export default MainContent;
