import React, { Component } from "react";
import "../styles/feedProfile.css";
import FeedPost from "./FeedPost";
import { connect } from "react-redux";

class Feed extends Component {
  render() {
    const posts = this.props.posts;
    return (
      <ul style={{ listStyle: "none" }}>
        {posts.map((post) => (
          <li>
            <FeedPost post={post} key={post.id} />
          </li>
        ))}
      </ul>
    );
  }
}

const mapStateToProps = (state) => ({
  posts: state.postsReducer.posts,
});

export default connect(mapStateToProps)(Feed);
