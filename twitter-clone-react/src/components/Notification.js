import React, { Component } from "react";
import Avatar from "./Avatar";

class Notification extends Component {
  render() {
    return (
      <div style={{ border: 1, borderStyle: "solid", padding: 17 }}>
        <div className="row">
          <Avatar />
          Username, 22.Feb
        </div>
        <div className="row">Bla bla bla bla bla bla bla</div>
      </div>
    );
  }
}

export default Notification;
