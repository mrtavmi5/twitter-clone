import React, { Component } from "react";

class Avatar extends Component {
  render() {
    return (
      <div>
        <img
          src="avatar.png"
          alt="avatar"
          width={40}
          height={40}
          style={{ borderRadius: 50 }}
        />
      </div>
    );
  }
}

export default Avatar;
