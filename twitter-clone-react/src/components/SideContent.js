import React, { Component } from "react";
import WhoToFollow from "./WhoToFollow";

class SideContent extends Component {
  render() {
    return (
      <div
        className="col"
        style={{ borderColor: "red", borderWidth: 1, borderStyle: "solid" }}
      >
        <WhoToFollow />
      </div>
    );
  }
}

export default SideContent;
