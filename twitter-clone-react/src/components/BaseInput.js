import React, { Component } from "react";

class BaseInput extends Component {
  render() {
    return (
      <label>
        <p className="label-txt">{this.props.label}</p>
        <input
          type={this.props.type}
          className="input"
          onChange={this.props.onChange}
        />
        <div className="line-box">
          <div className="line"></div>
        </div>
      </label>
    );
  }
}

export default BaseInput;
