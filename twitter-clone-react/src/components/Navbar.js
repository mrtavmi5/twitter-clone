import React from "react";
import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <Link to="/" className="navbar-brand">
        Twitter Clone
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarCollapse"
        aria-controls="navbarCollapse"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarCollapse">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <Link to="/" className="nav-link">
              Home
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/profile" className="nav-link">
              Profile
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/notifications" className="nav-link">
              Notifications
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/messages" className="nav-link">
              Messages
            </Link>
          </li>
          <Link to="/registration" className="nav-link">
            Registration
          </Link>
          <Link to="/login" className="nav-link">
            Login
          </Link>
        </ul>
      </div>
    </nav>
  );
}
