import React, { Component } from "react";

class NotificationButton extends Component {
  render() {
    return (
      <div>
        <link
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          rel="stylesheet"
        />
        <a className="fa fa-globe">
          <span className="fa fa-comment"></span>
          <span className="num">2</span>
        </a>
      </div>
    );
  }
}

export default NotificationButton;
