import React, { Component } from "react";
import Avatar from "./Avatar";

export default class FeedPost extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-0">
          <Avatar />
        </div>
        <div className="col-8">
          <div>{this.props.post.user.username}</div>
          <div>{this.props.post.content}</div>
        </div>
      </div>
    );
  }
}
