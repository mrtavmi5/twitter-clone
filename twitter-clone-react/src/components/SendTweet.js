import React, { Component } from "react";
import Avatar from "./Avatar";

class SendTweet extends Component {
  render() {
    return (
      <div className="row">
        <Avatar />
        <div className="col-10">
          <input
            className="form-control"
            type="text"
            placeholder="What's happening?"
          />
        </div>
        <div className="col">
          <button type="button" className="btn btn-primary">
            Tweet
          </button>
        </div>
      </div>
    );
  }
}

export default SendTweet;
