import Axios from "axios";

Axios.interceptors.request.use(
  function (config) {
    const jwtToken = localStorage.getItem("jwt");

    if (jwtToken) {
      config.headers["Authorization"] = "Bearer " + jwtToken;
    }
    return config;
  },
  function (err) {
    return Promise.reject(err);
  }
);
