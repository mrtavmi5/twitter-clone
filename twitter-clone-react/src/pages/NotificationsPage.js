import React, { Component } from "react";
import Notification from "../components/Notification";

class NotificationsPage extends Component {
  render() {
    return (
      <div style={{ border: 1, borderStyle: "solid" }}>
        <Notification />
        <Notification />
        <Notification />
        <Notification />
      </div>
    );
  }
}

export default NotificationsPage;
