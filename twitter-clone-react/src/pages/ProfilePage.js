import React, { Component } from "react";
import "../styles/feedProfile.css";

export default class ProfilePage extends Component {
  render() {
    return (
      <div>
        <div className="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded shadow-sm">
          <img
            className="mr-3"
            src="/docs/4.4/assets/brand/bootstrap-outline.svg"
            alt=""
            width="48"
            height="48"
          />
          <div className="lh-100">
            <h6 className="mb-0 text-white lh-100">Profile name</h6>
            <small>Followers 56</small>
            <br></br>
            <small>Following 111</small>
          </div>
        </div>
      </div>
    );
  }
}
