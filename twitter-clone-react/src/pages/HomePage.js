import React, { Component } from "react";
import MainContent from "../components/MainContent";
import SideContent from "../components/SideContent";
import { connect } from "react-redux";
import { getPosts } from "../redux/posts/actions";

class HomePage extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    return (
      <div className="container" style={({ margin: 0 }, { maxWidth: "100%" })}>
        <div className="row">
          <MainContent />
          <SideContent />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = { getPosts };

export default connect(null, mapDispatchToProps)(HomePage);
