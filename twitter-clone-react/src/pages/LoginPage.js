import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../styles/signin.css";
import { connect } from "react-redux";
import { login } from "../redux/auth/actions";

class LoginPage extends Component {
  state = { credentials: { username: "", password: "" } };

  onChange = (event) => {
    const field = event.target.name;
    const credentials = this.state.credentials;
    credentials[field] = event.target.value;
    return this.setState({ credentials: credentials });
  };

  onSave = (event) => {
    event.preventDefault();
    this.props.login(this.state.credentials);
  };

  render() {
    return (
      <div>
        <form className="form-signin">
          <img
            className="mb-4"
            src="/docs/4.5/assets/brand/bootstrap-solid.svg"
            alt=""
            width="72"
            height="72"
          />
          <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
          <label htmlFor="inputEmail" className="sr-only">
            Username
          </label>
          <input
            name="username"
            type="text"
            id="inputEmail"
            className="form-control"
            placeholder="Username"
            required
            autoFocus
            onChange={this.onChange}
          />
          <label htmlFor="inputPassword" className="sr-only">
            Password
          </label>
          <input
            name="password"
            type="password"
            id="inputPassword"
            className="form-control"
            placeholder="Password"
            required
            onChange={this.onChange}
          />
          <div className="checkbox mb-3">
            <label>
              <input type="checkbox" value="remember-me" /> Remember me
            </label>
          </div>
          <button
            className="btn btn-lg btn-primary btn-block"
            type="submit"
            onClick={this.onSave}
          >
            Sign in
          </button>
        </form>
        <div className="d-flex justify-content-center links">
          Don't have an account?
          <Link to="/registration" className="ml-2">
            Sign up
          </Link>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = { login };

export default connect(null, mapDispatchToProps)(LoginPage);
