import React, { Component } from "react";
import { connect } from "react-redux";
import BaseInput from "../components/BaseInput";
import { register } from "../redux/auth/actions";
import "../styles/registration.css";

class RegistrationPage extends Component {
  state = { username: "", password: "", confirmPassword: "" };

  getUsername = (event) => {
    const username = event.target.value;
    this.setState({ username });
  };

  getPassword = (event) => {
    const password = event.target.value;
    this.setState({ password });
  };

  getConfirmPassword = (event) => {
    const confirmPassword = event.target.value;
    this.setState({ confirmPassword });
  };

  onSubmit = (event) => {
    if (this.state.password !== this.state.confirmPassword) {
      alert("Passwords don't match");
    }

    const newUser = {
      username: this.state.username,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
    };

    this.props.register(newUser);
    event.preventDefault();
  };

  render() {
    return (
      <div>
        <form>
          <BaseInput
            label="ENTER YOUR USERNAME"
            type="text"
            onChange={this.getUsername}
          />

          <BaseInput
            label="ENTER YOUR PASSWORD"
            type="password"
            onChange={this.getPassword}
          />

          <BaseInput
            label="REPEAT YOUR PASSWORD"
            type="password"
            onChange={this.getConfirmPassword}
          />
          <div>
            <button type="submit" onClick={this.onSubmit}>
              submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default connect(null, { register })(RegistrationPage);
