import {
  GET_FOLLOWERS_SUCCESS,
  GET_FOLLOWERS_START,
  GET_FOLLOWERS_FAIL,
} from "./types";

const initialState = {
  followers: [],
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_FOLLOWERS_START:
      return {
        ...state,
      };
    case GET_FOLLOWERS_SUCCESS:
      return {
        ...state,
        followers: action.payload,
      };
    case GET_FOLLOWERS_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
}
