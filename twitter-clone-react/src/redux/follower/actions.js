import axios from "axios";
import {
  GET_FOLLOWERS_SUCCESS,
  GET_FOLLOWERS_START,
  GET_FOLLOWERS_FAIL,
} from "./types";

export const login = () => async (dispatch) => {
  dispatch({
    type: GET_FOLLOWERS_START,
  });
  try {
    const res = await axios.get("http://localhost:8080/user/all");
    dispatch({
      type: GET_FOLLOWERS_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_FOLLOWERS_FAIL,
      payload: error.response.data,
    });
  }
};
