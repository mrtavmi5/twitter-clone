import axios from "axios";
import {
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_START,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
} from "./types";

export const login = (user) => async (dispatch) => {
  dispatch({
    type: LOGIN_START,
  });
  try {
    const res = await axios.post("http://localhost:8080/login", user);
    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: LOGIN_FAIL,
      payload: error.response.data,
    });
  }
};

export const register = (user) => async (dispatch) => {
  dispatch({
    type: REGISTER_START,
  });
  try {
    const res = await axios.post("http://localhost:8080/register", user);
    dispatch({
      type: REGISTER_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: REGISTER_FAIL,
      payload: error.response.data,
    });
  }
};
