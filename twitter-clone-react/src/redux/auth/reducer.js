import {
  LOGIN_SUCCESS,
  LOGIN_START,
  LOGIN_FAIL,
  REGISTER_START,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
} from "./types";

const initialState = {
  jwt: null,
  submitting: false,
  registering: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN_START:
      return {
        ...state,
        submitting: true,
      };
    case LOGIN_SUCCESS:
      localStorage.setItem("jwt", action.payload.jwt);
      window.location.reload();

      return {
        ...state,
        jwt: action.payload.jwt,
        submitting: false,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        submitting: false,
        error: action.payload,
      };
    case REGISTER_START:
      return {
        ...state,
        registering: true,
      };

    case REGISTER_SUCCESS:
      window.location.replace(window.location.origin);
      return {
        ...state,
        registering: false,
      };
    case REGISTER_FAIL:
      return {
        ...state,
        registering: false,
        error: action.payload,
      };

    default:
      return state;
  }
}
