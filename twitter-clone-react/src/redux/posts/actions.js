import axios from "axios";
import { GET_POSTS_START, GET_POSTS_SUCCESS, GET_POSTS_FAIL } from "./types";

export const getPosts = () => async (dispatch) => {
  dispatch({
    type: GET_POSTS_START,
  });
  try {
    const res = await axios.get("http://localhost:8080/post/all");
    dispatch({
      type: GET_POSTS_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    console.log("error");
    console.log(error);
    dispatch({
      type: GET_POSTS_FAIL,
      payload: error.response.data,
    });
  }
};
