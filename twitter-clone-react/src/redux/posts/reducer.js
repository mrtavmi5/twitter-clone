import { GET_POSTS_SUCCESS, GET_POSTS_START, GET_POSTS_FAIL } from "./types";

const initialState = {
  posts: [],
  loadingPosts: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_POSTS_START:
      return {
        ...state,
        loadingPosts: true,
      };
    case GET_POSTS_SUCCESS:
      return {
        ...state,
        posts: action.payload,
        loadingPosts: false,
      };
    case GET_POSTS_FAIL:
      return {
        ...state,
        loadingPosts: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
