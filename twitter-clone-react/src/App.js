import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Redirect, Route } from "react-router-dom";
import "./App.css";
import Message from "./components/Message";
import Navbar from "./components/Navbar";
import "./interceptors";
import HomePage from "./pages/HomePage";
import LoginPage from "./pages/LoginPage";
import NotificationsPage from "./pages/NotificationsPage";
import ProfilePage from "./pages/ProfilePage";
import RegistrationPage from "./pages/RegistrationPage";
import store from "./Store";

function App() {
  const loggedIn = localStorage.getItem("jwt");

  return (
    <div className="App">
      <Provider store={store}>
        <Router>
          <Navbar />
          <Route exact path="/home">
            <Redirect to="/" />
          </Route>
          <Route exact path="/">
            {loggedIn ? <HomePage /> : <LoginPage />}
          </Route>
          <Route exact path="/profile" component={ProfilePage} />
          <Route exact path="/notifications" component={NotificationsPage} />
          <Route exact path="/messages" component={Message} />
          <Route exact path="/registration">
            {loggedIn ? <Redirect to="/" /> : <RegistrationPage />}
          </Route>
          <Route exact path="/login" component={LoginPage} />
        </Router>
      </Provider>
    </div>
  );
}

export default App;
